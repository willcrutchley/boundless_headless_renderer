from boundless_headless_renderer.renderer import BoundlessRenderer
import moderngl, os
import argparse

def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

def main():
	parser = argparse.ArgumentParser(description='Boundless Icon Renderer by @willcrutchley')
	parser.add_argument("-s", "--style", choices=["uniform", "greedy"], default="greedy", required=False)
	parser.add_argument("-r", "--resolution", required=True, help="Specify the render resolution (e.g 256 for a 256x256 image)")
	parser.add_argument("-a", "--anti-alias", action="store_true", help="Renders at a slightly higher resolution and then downscales (helps to clean up edges)")
	parser.add_argument("-o", "--overwrite", action="store_true", help="Renders all specified objects even if their image already exists in the output directory")
	parser.add_argument("--force-foliage", action="store_true", help="Forces the rendering of blocks with treeFoliage (disabled by default since the script does not correctly render these)")
	parser.add_argument("-q", "--quiet", action="store_true", help="No printing")
	parser.add_argument("-g", "--boundless-path", required=True, help="Path to MacOS Boundless install", type=dir_path)
	parser.add_argument("--force-egl", action="store_true", help="Forces the renderer to use an EGL backend (use if attempting to run on a headless linux machine)")

	render_group = parser.add_argument_group(title="What to render", description="Pick a specific ID or NAME or one or more of [--items, --blocks, --props]")
	render_override = render_group.add_mutually_exclusive_group()
	render_override.add_argument("--id", help="Specify an ID to render (find it in compileditems or compiledblocks)")
	render_override.add_argument("--name", help="Specify a NAME to render (find it in compileditems or compiledblocks). May end up rendering the DUGUP version")
	render_group.add_argument("-i", "--items", action="store_true", help="Render all items")
	render_group.add_argument("-p", "--props", action="store_true", help="Render all props")
	render_group.add_argument("-b", "--blocks", action="store_true", help="Render all blocks")
	render_group.add_argument("-l", "--liquids", action="store_true", help="Render all liquids")

	colour_group = parser.add_argument_group(title="Colouration",
		description=
		"""
		By default the script will render all colours.
		This can take a long time for blocks with 255 possible base colours.
		\nTherefore you can specify a specific base colour to only render that one (this doesn't apply to anything using a palette with <255 options)
		"""
	)
	colour_group.add_argument("-bc", "--base-colour", "--base-color")

	args = parser.parse_args()
	argvars = vars(args)
	if not (args.items or args.props or args.blocks or args.liquids) and not (args.id or args.name):
		parser.error('Specify at least one type of object to render (--items, --props, --blocks, --liquids)')

	if args.liquids:
		parser.error("Liquids are currently disabled.")

	os.environ["BOUNDLESS_PATH"] = argvars["boundless_path"]

	if argvars["force_egl"]:
		ctx = moderngl.create_context(standalone=True, backend="egl")
	else:
		ctx = moderngl.create_context(standalone=True)

	renderer = BoundlessRenderer(os.environ["BOUNDLESS_PATH"], os.path.dirname(__file__), ctx, argvars)
	scenes = {}
	if argvars["items"] or argvars["id"] or argvars["name"]:
		items = renderer.discover_items()
		item_scenes = renderer.scenes_from_renderables(items)
		scenes.update(item_scenes)
	if argvars["props"] or argvars["id"] or argvars["name"]:
		props = renderer.discover_props()
		prop_scenes = renderer.scenes_from_renderables(props)
		scenes.update(prop_scenes)
	if argvars["blocks"] or argvars["id"] or argvars["name"]:
		blocks = renderer.discover_blocks()
		block_scenes = renderer.scenes_from_renderables(blocks)
		scenes.update(block_scenes)

	# if argvars["liquids"] or argvars["id"] or argvars["name"]:
		# Liquids just treated as blocks with transparency
		# CURRENTLY DISABLED
		# discover_data = renderer.discover_liquids()
		# liquids = {b: c[0] for b, c in discover_data.items()}
		# liquid_colour_overrides = {b: c[1] for b, c in discover_data.items()}
		# liquid_scenes = renderer.scenes_from_renderables(liquids)
		# scenes.update(liquid_scenes)

	for id, scene in scenes.items():
		checkpath = "out/" + scene.name
		# Naively assume that if the scene's directory exists and isn't
		# empty we can ignore it
		if os.path.exists(checkpath) and os.listdir(checkpath) and not argvars["overwrite"]:
			print("[INFO] Skipping {}".format(scene.name))
		else:
			# if scene.renderable.type_name == "BLOCK" and scene.renderable.tex_atlas == "liquidatlas":
			# 	renderer.palette_json[1]["colorVariations"] = [liquid_colour_overrides[id]]

			base_palette = renderer.palette_json[scene.renderable.base_palette]
			base_ids = base_palette['colorVariations'] if base_palette['colorVariations'] else [0]
			decal_palette = renderer.palette_json[scene.renderable.decal_palette]
			decal_ids = decal_palette['colorVariations'] if decal_palette['colorVariations'] else [0]

			if scene.renderable.type_name == "PROP" and not scene.renderable.decal_texture_path: decal_ids = [0]

			base_override = False
			if len(base_ids) == 255 and argvars["base_colour"]:
				base_ids = [argvars["base_colour"]]
				base_override = True

			for b in range(0, len(base_ids)):
				if not base_override:
					col_id = b
					dec_id = b
				else: 
					col_id = int(base_ids[0])
					dec_id = 0
				

				if len(base_ids) != len(decal_ids):
					dec_id = 0

				renderer.render_scene(scene, col_id, dec_id)

if __name__ == "__main__":
	main()
